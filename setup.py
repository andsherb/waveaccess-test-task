from setuptools import find_packages, setup
setup(
    name='summarizer',
    packages=find_packages(include=['summarizer']),
    version='0.1.0',
    description='',
    author='A. Shcherbinin',
    author_email='andsherb@gmail.com',
    license='MIT',
    project_urls={'Source': 'https://gitlab.com/andsherb/waveaccess-test-task'},
    install_requires=['pandas',
                      'numpy',
                      'pytest',
                      'openpyxl',
                      'tabulate'
                      ],
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    test_suite='tests',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ]
)
