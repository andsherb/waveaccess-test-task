#!/usr/bin/python

from typing import Optional
import warnings
import pandas as pd
import numpy as np
from .utils import *

warnings.filterwarnings("ignore")


class Summarizer:
    """
    Take pandas dataframe as input, iterate through each of the columns in the
    dataframe and based on column datatype, create summary statistics for each,
    and print them out in a table with chosen format (xlsx, html, markdown)

    Parameters
    ----------
    df: pd.Dataframe
        Pandas Dataframe
    output_type: Optional[str]
        Table output type:
        'markdown', 'html', 'xlsx'
        Default: 'markdown'
    output_path:  str, path object
    drop_cols: Optional[list]
        list of column names in dataframe which you do not want to summarize
        Default: None
    """

    def __init__(
        self,
        df: pd.DataFrame,
        output_type: Optional[str],
        output_path: FilePath,
        drop_cols: Optional[list] = None,
    ) -> None:
        if not isinstance(df, pd.DataFrame):
            raise TypeError(
                f"This summarizer works only with pd.Dataframe, you gave {type(df)}"
            )

        if drop_cols:
            self.df = df.drop(columns=drop_cols)
        else:
            self.df = df

        if output_type:
            self.output_type = output_type
        else:
            self.output_type = "markdown"

        self.output_path = output_path

    def _describe_features(
        self, include_type
    ) -> (Optional[pd.DataFrame], Optional[pd.DataFrame]):
        """
        Generate descriptive statistics the same with pd.describe dependind on type:
        -Numeric
        -Categorical
        -Bool
        -Datetime

        Parameters
        ----------
        include_type:
        - np.numeric: Numeric columns,
        - 'object': Categorical columns
        - bool: Boolean columns
        - np.datetime64: Datetime columns

        Returns:
        -------
        Descriptive statistics dataframe and dataframe with columns
        corresponding to type.
        If there are no columns with "type" in dataframe, it returns None, None
        """
        try:
            describe_values = self.df.describe(include=include_type)
        except ValueError:
            return None, None
        features = self.df[describe_values.columns]
        describe_values.loc["col_type"] = features.dtypes
        return describe_values, features

    def describe_numeric_features(self) -> Optional[pd.DataFrame]:
        """
        Generate descriptive statistics for a Numeric type columns in the dataframe
        Includes:
        'count', 'mean', 'std', 'minimum', quartiles ('25%', '50%', '75%'),
        'maximum', 'column type', 'skew', 'mad', 'kurtosis', 'var', 'coefficient of variation(cv)',
        'interfertile range (iqr)', 'first mode', 'percent of zeros'

        Returns:
        -------
        Numeric descriptive statistics dataframe
        or None if there no Numeric columns in the dataframe
        """
        describe_numeric, numeric_features = self._describe_features(np.number)
        if describe_numeric is None:
            return None
        describe_numeric = pd.concat(
            [describe_numeric, numeric_features.agg(["skew", "mad", "kurt", "var"])]
        )
        describe_numeric.loc["cv"] = (
            describe_numeric.loc["std"] / describe_numeric.loc["mean"]
        )
        describe_numeric.loc["iqr"] = (
            describe_numeric.loc["75%"] - describe_numeric.loc["25%"]
        )
        describe_numeric.loc["first_mode"] = numeric_features.agg("mode").iloc[0]
        describe_numeric.loc["percent_zeros"] = (
            (numeric_features == 0).sum() / describe_numeric.loc["count"] * 100
        )
        return describe_numeric

    def describe_bool_features(self) -> Optional[pd.DataFrame]:
        """
        Generate descriptive statistics for a Bool type columns in the dataframe
        Includes:
        'count', 'unique', 'column type', False counts, True counts

        Returns:
        -------
        Bool descriptive statistics dataframe or None if there no Numeric columns in the dataframe
        """
        describe_bool, bool_features = self._describe_features(bool)
        if describe_bool is None:
            return None

        bool_features = self.df[describe_bool.columns]
        describe_bool = pd.concat([describe_bool, bool_features.agg(pd.value_counts)])
        return describe_bool.drop(index=["freq", "top"])

    def describe_cat_features(self) -> Optional[pd.DataFrame]:
        """
        Generate descriptive statistics for a Categorical type columns in the dataframe
        Includes:
        'count', 'unique', 'top counts', 'freq of top counts',
        'column type', 'percent of zeros (rows with zero length)'

        Returns:
        -------
        Categorical descriptive statistics dataframe
        or None if there no Numeric columns in the dataframe
        """
        describe_cat, cat_features = self._describe_features("object")
        if describe_cat is None:
            return None
        describe_cat.loc["percent_zeros"] = (
            cat_features.applymap(lambda x: len(x) == 0).sum()
            / describe_cat.loc["count"]
            * 100
        )
        return describe_cat

    def describe_datetime_features(self) -> Optional[pd.DataFrame]:
        """
        Generate descriptive statistics for a Datetime type columns in the dataframe
        Includes:
        'count', 'unique', 'top counts', 'freq of top counts', 'first date',
        'last date', 'column type', 'percent_zeros (1970-01-01 00:00:00)'

        Returns:
        -------
        Datetime descriptive statistics dataframe
         or None if there no Numeric columns in the dataframe
        """
        describe_datetime, time_features = self._describe_features(np.datetime64)
        if describe_datetime is None:
            return None
        describe_datetime.loc["percent_zeros"] = (
            time_features.applymap(
                lambda x: x == np.datetime64("1970-01-01 00:00:00")
            ).sum()
            / describe_datetime.loc["count"]
            * 100
        )
        return describe_datetime

    def save_summary(self) -> None:
        """
        Creates summary statistics for each column in dataframe,
        and print them out in a tables.
        """
        # create dict with Types as keys and Summarization results as values
        summary_dict = {
            "Numeric": self.describe_numeric_features(),
            "Categorical": self.describe_cat_features(),
            "Bool": self.describe_bool_features(),
            "Datetime": self.describe_datetime_features(),
        }
        # save results in chosen output type
        if self.output_type == "markdown":
            to_markdown(summary_dict, self.output_path)
        elif self.output_type == "xlsx":
            to_xlsx(summary_dict, self.output_path)
        elif self.output_type == "html":
            to_html(summary_dict, self.output_path)
        else:
            raise TypeError("Wrong output type")
