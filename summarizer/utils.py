#!/usr/bin/python

from pandas._typing import FilePath
import pandas as pd


def to_markdown(summary_dict: dict, path: FilePath):
    """
    Saves results to markdown file
    """
    if any(1 for x in summary_dict.values() if x is not None):
        markdown = ""
        for key, value in summary_dict.items():
            if value is not None:
                markdown += f"\n\n{key} features summary \n" + value.to_markdown(
                    tablefmt="grid"
                )
                print(f"{key} features described")
        with open(path, "w", encoding='utf-8') as file:
            file.write(markdown)
    else:
        raise ValueError("Something wrong, all columns are emtpy")


def to_xlsx(summary_dict: dict, path: FilePath) -> None:
    """
    Saves results to excel file
    """

    if any(1 for x in summary_dict.values() if x is not None):
        with pd.ExcelWriter(path) as writer:
            for key, value in summary_dict.items():
                if value is not None:
                    value.to_excel(writer, sheet_name=key)
                    print(f"{key} features described")
    else:
        raise ValueError("Something wrong, all columns are emtpy")


def generate_one_table_html(table_html: str, name: str) -> str:
    """
    Generates html code with pandas table converted to html using JS script

    Parameters:
    -----------
    table_html: str
        pd.DataFrame.to_html() string
    name: str
        html table name

    Returns:
        html_code: str
    """
    html = f"""
    <div>
    <h2>{name} features summarize</h2>
    </div>
    {table_html} <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" 
    integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script> <script 
    type="text/javascript" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script> <script> $(
    document).ready( function () {{ $('#table_{name}').DataTable({{ 
                paging: true,    
                // scrollY: 400,
            }});
        }});
    </script>
    """
    # return the html
    return html


def generate_html(html_tables: str) -> str:
    """
    Generate full html page code with concatenated tables

    Parameters:
    -----------
    html_tables: str
        concatenated html code tables after using generate_one_table_html()

    Returns:
        html_code: str
    """
    html = f"""
    <html>
    <header>
        <link href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css" rel="stylesheet">
    </header>
    <body>
    {html_tables}
    </body>
    </html>
    """
    return html


def to_html(summary_dict: dict, path: FilePath) -> None:
    """
    Saves results to html page file
    """

    if any(1 for x in summary_dict.values() if x is not None):
        html_summary = ""
        for key, value in summary_dict.items():
            if value is not None:
                table_html = value.to_html(table_id=f"table_{key}")
                html_summary += generate_one_table_html(table_html, key)
                print(f"{key} features described")

        html = generate_html(html_summary)
        with open(path, "w", encoding='utf-8') as html_file:
            html_file.write(html)
    else:
        raise ValueError("Something wrong, all columns are emtpy")
