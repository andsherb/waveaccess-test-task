# from summarizer.summarizer import Summarizer
import pandas as pd
import numpy as np
import pytest
from summarizer import Summarizer
import warnings
warnings.filterwarnings("ignore")


@pytest.fixture
def test_summarizer():
    nums1 = [1, 2, 3, 4, 5, 6]
    nums2 = [7, 8, 9, 10, 11, 12]

    cat1 = ['a', 'a', 'b', 'c', 'b', 'b']
    cat2 = ['e', 'f', '', '', 'e', '']

    bool1 = [True, True, False, True, False, True]
    bool2 = [True, False, False, False, True, True]

    dt1 = pd.date_range(np.datetime64('2000-01-01'), periods=6, freq='d')
    dt2 = [np.datetime64('1970-01-01')] * 6

    test_df = pd.DataFrame([nums1, nums2, cat1, cat2, bool1, bool2, dt1, dt2]).T
    test_df.columns = ('nums_1', 'nums_2', 'cat_1', 'cat_2',
                       'bool_1', 'bool_2', 'dt_1', 'dt_2')

    test_df.nums_1 = test_df.nums_1.astype(int)
    test_df.nums_2 = test_df.nums_2.astype(int)

    test_df.bool_1 = test_df.bool_1.astype(bool)
    test_df.bool_2 = test_df.bool_2.astype(bool)

    return Summarizer(test_df, 'test', 'test.md')


def test_empty_dataframe():
    summarizer = Summarizer(pd.DataFrame(), 'markdown', 'markdown.md')
    with pytest.raises(ValueError):
        summarizer.save_summary()


def test_wrong_data_type():
    with pytest.raises(TypeError):
        Summarizer([1, 2, 3], 'markdown', 'markdown.md')


def test_numeric_features(test_summarizer):
    summary = test_summarizer.describe_numeric_features()
    assert summary.loc['mean', 'nums_1'] == 3.5
    assert summary.loc['max', 'nums_2'] == 12
    assert len(summary.columns) == 2


def test_categorical_features(test_summarizer):
    summary = test_summarizer.describe_cat_features()
    assert summary.loc['count', 'cat_1'] == 6
    assert summary.loc['percent_zeros', 'cat_2'] == 50.0


def test_bool_features(test_summarizer):
    summary = test_summarizer.describe_bool_features()
    assert summary.loc[False, 'bool_1'] == 2
    assert summary.loc[True, 'bool_2'] == 3


def test_datetime_features(test_summarizer):
    summary = test_summarizer.describe_datetime_features()
    assert summary.loc['unique', 'dt_2'] == 1
    assert summary.loc['first', 'dt_1'] == np.datetime64('2000-01-01 00:00:00')


def test_wrong_output_type(test_summarizer):
    with pytest.raises(TypeError):
        test_summarizer.save_summary()


def test_save_file(test_summarizer):
    test_summarizer.output_type = 'markdown'
    test_summarizer.save_summary()
    with open('tests/test.md') as f:
        text = f.readlines()
    assert text[2] == 'Numeric features summary \n'
