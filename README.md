Summarizer test task
==============================

Background In data analysis we are operating with large dataframes, which can contain different
types of data, such as binary, numeric, datetime and classification variables.
Manual analysis of such dataframes is not efficient, and it could be very helpful to have some
statistical overview of the dataframe.

## Where to get it

The source code is currently hosted on GitLab at: https://gitlab.com/andsherb/waveaccess-test-task

## Installation

For an installation a module into your virtual environment
in the same directory after cloning the git repo, execute:

``pip install waveaccess-test-task``

## Example

``from summarizer import Summarizer``

``summarizer = Summarizer(dataframe, 'markdown', 'tables.md')``

``summarizer.save_summary()``


Project Organization
------------


    ├── README.md          <- The top-level README for developers using this project.
    │
    ├── notebooks          <- Jupyter notebooks
    │
    ├── tests              <- Test code
    │
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── summarizer                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes summarizer a Python module
    │   │
    │   ├── summarizer.py 
    │   │
    │   ├── utils.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io
--------

